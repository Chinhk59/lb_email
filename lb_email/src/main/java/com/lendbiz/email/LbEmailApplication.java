package com.lendbiz.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LbEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(LbEmailApplication.class, args);
		
	}

}
