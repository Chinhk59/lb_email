package com.lendbiz.email.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MsgResponse {

	private String from;
	private String subject;
	private Date sendTime;
	private String custId;
	private long amount;
	private long befBalance;
	private long avlBalance;
	private String content;
	private String txDate;
	
}
