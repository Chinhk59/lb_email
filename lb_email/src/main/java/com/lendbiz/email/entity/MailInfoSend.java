package com.lendbiz.email.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "MAIL_INFO_SENDER")
@NamedQuery(name = "MailInfoSend.findAll", query = "SELECT a FROM MailInfoSend a")
@Setter
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailInfoSend {

	@Id
	private String id;
	
	@Column(name = "USERNAME")
	private String userName;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "BANKCODE")
	private String bankCode;
	
	@Column(name = "BANKNAME")
	private String bankName;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "MAILRECID")
	private String mailRecId;
}
