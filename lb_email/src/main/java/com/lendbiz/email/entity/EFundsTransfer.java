package com.lendbiz.email.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "E_FUNDS_TRANSFER")
@NamedQuery(name = "EFundsTransfer.findAll", query = "SELECT g FROM EFundsTransfer g")
@Setter
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EFundsTransfer {
	@Id
	private String id;

	@Column(name = "CUSTID")
	private String custId;

	@Column(name = "SENDER")
	private String sender;

	@Column(name = "SUBJECT")
	private String subject;

	@Column(name = "CONTENT")
	private String content;

	@Column(name = "SENDDATE")
	private Date sendDate;

	@Column(name = "TXDATE")
	private Date txDate;

	@Column(name = "BEFBALANCE")
	private Long befBalance;

	@Column(name = "AMOUNT")
	private Long amount;

	@Column(name = "AVLBALANCE")
	private Long avlBalance;

	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "DESCRIPTION")
	private String description;
}
