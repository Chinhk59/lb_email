package com.lendbiz.email.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lendbiz.email.entity.CfMast;

@Repository("cfMastRepository")
public interface CfMastRepository extends JpaRepository<CfMast, String> {
	
//	@Procedure(procedureName = "GRREQJOIN")
//	public String register( String pv_Type,  String pv_Fullname,
//			String Pv_IDCode,  String Pv_IDDate,  float Pv_Revenue,
//			 String Pv_exFullname, String Pv_Address, String Pv_DateOfBirth,
//			 String Pv_exMobile, String Pv_Email, String Pv_Phone,
//			 float Pv_LoanLimit, String Pv_RefIDCode, String Pv_RefIDDate,
//			 String Pv_RefIDPlace, String Pv_RefName, String Pv_RelationAddress,
//			 String Pv_Time2rc);
	
//	@Query(value = "SELECT * FROM CFMAST where CUSTID = ?1", nativeQuery = true)
	Optional<CfMast>	findByCustid(String custId);
	
	//get cfmast voi truong hop la nguoi vay von
	@Query(value = "SELECT c.* FROM CFMAST c ,ODMAST o WHERE c.mobilesms = ?1 "
			+ "and o.afacctno = c.custid and o.orstatus = 'G' and o.orderid = o.borgorderid", nativeQuery = true)
	List<CfMast> findByPhone(String phone);
	
	//get cfmast vs truong hop la nha dau tu
	@Query(value = "select * from cfmast where mobilesms = ?1 and custtype = 'I'", nativeQuery = true)
	List<CfMast> findByMobileSms(String phone);
}
