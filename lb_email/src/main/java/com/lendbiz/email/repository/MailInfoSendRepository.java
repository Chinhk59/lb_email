package com.lendbiz.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lendbiz.email.entity.MailInfoSend;

public interface MailInfoSendRepository extends JpaRepository<MailInfoSend, Long> {

	List<MailInfoSend> findByMailRecId(String id);
}
