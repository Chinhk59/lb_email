package com.lendbiz.email.repository;

import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.lendbiz.email.entity.EFundsTransfer;

@Repository
public interface EFundsTransferRespository extends JpaRepository<EFundsTransfer, String> {

//	@Query(value = "UPDATE E_FUNDS_TRANSFER SET STATUS = ?1 WHERE CUSTID = ?2", nativeQuery = true)
//	void updateStatus(String status, String custId);
	
	@Transactional
	@Modifying
	@Query("UPDATE EFundsTransfer SET status = :status, description = :description WHERE custId = :custId")
	void updateStatus(
			@Param("status") String status, 
			@Param("custId") String custId,
			@Param("description") String description);
	
	
//	@Query(value = "SELECT * FROM E_FUNDS_TRANSFER where CUSTID = ?1 AND TO_CHAR(CAST(SENDDATE AS DATE), 'DD-MON-YY hh:mm:ss') = TO_CHAR(CAST(?2 AS DATE), 'DD-MON-YY hh:mm:ss')", nativeQuery = true)
	@Query(value = "SELECT * FROM E_FUNDS_TRANSFER where CUSTID = ?1 AND SENDDATE = ?2", nativeQuery = true)
	Optional<EFundsTransfer> findByCustIdAndSendDate(String custId, Date sendTime);
}
