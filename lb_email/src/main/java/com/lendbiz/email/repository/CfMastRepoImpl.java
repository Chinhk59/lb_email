package com.lendbiz.email.repository;

import java.sql.Types;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import com.lendbiz.email.dto.request.MsgResponse;
import com.lendbiz.email.entity.CfMast;
import com.lendbiz.email.service.BaseService;
import com.lendbiz.email.utils.Utils;

@Service
public class CfMastRepoImpl extends BaseService implements CfMastRepo {

	public CfMastRepoImpl(Environment env) {
		super(env);
	}

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int makeTransfer(CfMast cfmast, MsgResponse msg) {
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName("FUNDS_TRANSFER")					
				.declareParameters(new SqlParameter("Pv_Custid", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_Fullname", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_Address", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_RefIDCode", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_RefIDDate", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_RefIDPlace", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_Amount", Types.VARCHAR))
				.declareParameters(new SqlParameter("Pv_Content", Types.VARCHAR))
				.declareParameters(new SqlOutParameter("p_err_code", Types.INTEGER));

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("Pv_Custid", cfmast.getCustid());
		params.addValue("Pv_Fullname", cfmast.getFullName());
		params.addValue("Pv_Address", cfmast.getAddress());
		params.addValue("Pv_RefIDCode", cfmast.getIdCode());
		params.addValue("Pv_RefIDDate", Utils.convertDateToString(cfmast.getIdDate()));
		params.addValue("Pv_RefIDPlace", cfmast.getIdPlace());
		params.addValue("Pv_Amount", String.valueOf(msg.getAmount()));
		params.addValue("Pv_Content", "Nộp tiền mặt");

		// Exec stored procedure
		Map<String, Object> map = jdbcCall.execute(params);
		String result = map.get("p_err_code").toString();
		
		return Integer.parseInt(result);
	}

}
