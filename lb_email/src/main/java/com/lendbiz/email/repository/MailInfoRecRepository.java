package com.lendbiz.email.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lendbiz.email.entity.MailInfoRec;

@Repository
public interface MailInfoRecRepository extends JpaRepository<MailInfoRec, Long>{

	List<MailInfoRec> findAll();
}
