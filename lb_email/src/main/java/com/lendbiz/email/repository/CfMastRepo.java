package com.lendbiz.email.repository;

import com.lendbiz.email.dto.request.MsgResponse;
import com.lendbiz.email.entity.CfMast;

public interface CfMastRepo {

	public int makeTransfer(CfMast cfmast, MsgResponse msg);
}
