package com.lendbiz.email.constants;

public class Constants {
	public static final String TEXT_BL_AMOUNT = "Số dư trước giao dịch/ Balance before transaction: VND";
	public static final String TEXT_AMOUNT_TRANSACTION = "Số tiền giao dịch/ Transaction amount: VND";
	public static final String TEXT_AVL_AMOUNT= "Số dư khả dụng hiện thời/ Available balance: VND";
	public static final String TEXT_TXDATE = "Ngày giao dịch/ Transaction date:";
	public static final String TEXT_CONTENT = "Nội dung/ Narrative:";
	public static final String TEXT_END_CONTENT = "Nếu quý khách có bất cứ thắc mắc gì với nội dung thông báo trên";
	public static final String SQL_NULL = "null";
	public static final String STATUS_PROCESSING = "PROCESSING";
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAIL = "FAIL";
	public static final String CD_TYPE = "EM";
	public static final String CD_NAME_USER = "USER";
	public static final String CD_NAME_PASS = "PASSWORD";
	public static final String TEXT_CONTENT_FUNDS = "CHUYEN VAO TAI KHOAN";
	public static final String TEXT_END_FUNDS = "MO TAI LENDBIZ";
	public static final String TEXT_END_FUNDS2 = "MO\r\nTAI LENDBIZ";
}
