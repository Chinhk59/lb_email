package com.lendbiz.email.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lendbiz.email.constants.JsonMapper;
import com.lendbiz.email.entity.MailInfoRec;
import com.lendbiz.email.entity.MailInfoSend;
import com.lendbiz.email.model.exception.BusinessException;
import com.lendbiz.email.model.exception.InputInvalidExeption;
import com.lendbiz.email.repository.MailInfoRecRepository;
import com.lendbiz.email.repository.MailInfoSendRepository;
import com.lendbiz.email.service.EmailService;

@RestController
@RequestMapping("/email/v1.0")
public class EmailController extends AbstractController<EmailService> {
	
	@Autowired
	private MailInfoRecRepository mailInfoRecRepo;
	
	@Autowired
	private MailInfoSendRepository mailInfoSendRepo;
	
	@PostMapping("/getmail")
	@Scheduled( initialDelay = 1 * 60 , fixedDelay = 2 * 60 * 1000)
	public ResponseEntity<?> register() throws BusinessException {
		// get info mail reception
		List<MailInfoRec> mailRecs = mailInfoRecRepo.findAll();
		logger.info("[B0] List mail reception: {}", JsonMapper.writeValueAsString(mailRecs));
		if (mailRecs.isEmpty())
			throw new InputInvalidExeption("02", "Info email reciption is invalid");
		
		for (MailInfoRec mailInfoRec : mailRecs) {
			// get list mail sender
			List<MailInfoSend> mailSends = mailInfoSendRepo.findByMailRecId(mailInfoRec.getId());
			logger.info("[B0] List mail sendder: {}", JsonMapper.writeValueAsString(mailSends));
			if (mailSends.isEmpty())
				continue;
			List<String> senders = mailSends.stream().map(mail -> new String(mail.getEmail()))
					.collect(Collectors.toList());

			// call getmail()
			service.getmail(mailInfoRec.getUserName(), mailInfoRec.getPassword(), senders);
		}

		return response(toResult("Success"));
	}
	
}
