package com.lendbiz.email.model.base;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.lendbiz.email.constants.ErrorCode;
import com.lendbiz.email.dto.response.LbResponse;
import com.lendbiz.email.utils.StringUtil;

public abstract class BaseController {
	/*-----------------------------------------------
	  * Property
	  *-----------------------------------------------*/
	  /**
	   * Logger
	   */
	  protected Logger logger = LogManager.getLogger(this.getClass());

	  /*-----------------------------------------------
	  * Protected
	  *-----------------------------------------------*/

	  /**
	   * Create response
	   *
	   * @param response
	   * @return response
	   */

	  
	  protected <T> ResponseEntity<LbResponse<T>> response(LbResponse<T> response) {
	      if (response == null) {
	        throw new IllegalArgumentException("Please set responseBean.");
	      }

	      if (StringUtil.isEmty(response.getStatus())) {
	        response.setStatus(ErrorCode.SUCCESS);
	      }
	      return new ResponseEntity<LbResponse<T>>(response, HttpStatus.OK);
	    }

	  /**
	   * create file response
	   *
	   * @param fileName file name
	   * @param file byte array
	   * @return response
	   * @throws Exception
	   */
	  protected ResponseEntity<byte[]> response(String fileName, byte[] file)
	      throws Exception {
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Content-Disposition", "attachment; filename*=UTF-8''"
	        + URLEncoder.encode(fileName, StandardCharsets.UTF_8.name()));

	    // Create response
	    return ResponseEntity.ok().headers(headers).contentLength(file.length)
	        .contentType(
	            MediaType.parseMediaType("application/octet-stream"))
	        .body(file);
	  }

}
