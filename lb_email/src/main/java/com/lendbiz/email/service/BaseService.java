/***************************************************************************
 * Copyright 2018 by VIETIS - All rights reserved.                *    
 **************************************************************************/
package com.lendbiz.email.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

public class BaseService {

    public BaseService(Environment env) {
    // TODO Auto-generated constructor stub
  }

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

}
