package com.lendbiz.email.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.SearchTerm;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.lendbiz.email.constants.Constants;
import com.lendbiz.email.constants.JsonMapper;
import com.lendbiz.email.dto.request.MsgResponse;
import com.lendbiz.email.entity.CfMast;
import com.lendbiz.email.entity.EFundsTransfer;
import com.lendbiz.email.repository.CfMastRepo;
import com.lendbiz.email.repository.CfMastRepository;
import com.lendbiz.email.repository.EFundsTransferRespository;
import com.lendbiz.email.service.BaseService;
import com.lendbiz.email.service.EmailService;
import com.lendbiz.email.utils.StringUtil;
import com.lendbiz.email.utils.Utils;
import com.sun.mail.pop3.POP3Store;


@Service("emailService")
public class EmailServiceImpl extends BaseService implements EmailService {

	String host = "mail.lendbiz.vn";  
	String mailStoreType = "pop3";  
	
	@Autowired
	private EFundsTransferRespository fundsTransfer;
	
	@Autowired
	private CfMastRepo cfMastRepo;
	
	@Autowired
	private CfMastRepository cfMastRepository;
	
	public EmailServiceImpl(Environment env) {
		super(env);
	}

	@Override
	public String getmail(String username, String password, List<String> senders) {
		try {
			// 1) get the session object
			Properties properties = new Properties();
			properties.put("mail.pop3.host", host);
			Session emailSession = Session.getDefaultInstance(properties);

			// 2) create the POP3 store object and connect with the pop server
			POP3Store emailStore = (POP3Store) emailSession.getStore(mailStoreType);
			logger.info("[B1] Start connect to server with user: {} password: {}", username, password);
			emailStore.connect(username, password);

			// 3) create the folder object and open it
			Folder emailFolder = emailStore.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);
			
			// 4) retrieve the messages from the folder in an array and print it
			SearchTerm term = new SearchTerm() {
				private static final long serialVersionUID = 1L;

				public boolean match(Message message) {
					try {
						if (!ObjectUtils.isEmpty(message.getSentDate()) && Utils.isCompareDate(message.getSentDate())) {
							return true;
						}
					} catch (MessagingException ex) {
						logger.info("error : {}", ex.getMessage());
					}
					return false;
				}
			};

            Message[] messages = emailFolder.search(term);

	        logger.info("[B2] message reception after filter: [size] {}", JsonMapper.writeValueAsString(messages.length));
	            List<MsgResponse> listMsg = new ArrayList<MsgResponse>();
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				
				String objFrom = JsonMapper.writeValueAsString(message.getFrom()[0]);
				JSONObject jsonObject = new JSONObject(objFrom);
				String from = jsonObject.get("address").toString();
				logger.info("[B2] from: {}", from);
				logger.info("[B2] senders: {}", senders);
				
				if (senders.contains(from.trim())) {
					
					MsgResponse msg = getDataFromMessage(message, from);
					if (!StringUtil.isEmty(msg.getCustId()) && Utils.isNumber(msg.getCustId())) {
						listMsg.add(msg);
					}
				}
			}

			logger.info("[B2] list mail reception is : {}", JsonMapper.writeValueAsString(listMsg));
			List<MsgResponse> listUnFunds = saveObjectToDB(listMsg);
			makeTransfer(listUnFunds);
			// 5) close the store and folder objects
			emailFolder.close(false);
			emailStore.close();
			logger.info("[B5] End get mail with mail: {}", username);

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void makeTransfer(List<MsgResponse> listMsg) {
		logger.info("[B4] Start makes transfer size {} value {}", listMsg.size(),
				JsonMapper.writeValueAsString(listMsg));
		if (!listMsg.isEmpty()) {
			for (MsgResponse msg : listMsg) {
				if (StringUtil.isEmty(msg.getCustId()))
					continue;
				if (msg.getAmount() == -1) {
					fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(),
							"Can not convert amount to number");
				}
				CfMast cfmast = new CfMast();
				if (msg.getCustId().length() == 6) {
					// get cfmast with custid = cif
					Optional<CfMast> optCfmast = cfMastRepository.findByCustid(msg.getCustId());
					if (!optCfmast.isPresent()) {
						logger.info("[B4] Can not find user with custid : {}",
								JsonMapper.writeValueAsString(msg.getCustId()));
						fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(), "Can not find user in DB");
					} else {
						cfmast = optCfmast.get();
					}
				} else if (msg.getCustId().length() == 10) {
					// get thong tin nguoi vay von
					List<CfMast> cfmasts = cfMastRepository.findByPhone(msg.getCustId());
					boolean isCheck = true;
					if (cfmasts.size() == 0) {
						//get thong tin nha dau tu
						List<CfMast> cfmastDt = cfMastRepository.findByMobileSms(msg.getCustId());
						if (cfmastDt.size() == 0) {
							fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(),
									"Can not find user in DB");
							continue;
						} else if (cfmastDt.size() == 1) {
							isCheck = false;
							cfmast = cfmastDt.get(0);
						} else {
							fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(),
									"There is more than one contract with the custid");
							continue;
						}

					} else if (cfmasts.size() == 1) {
						cfmast = cfmasts.get(0);
					} else {
						for (int i = 0; i < cfmasts.size() - 1; i++) {
							if (!cfmasts.get(i).getCustid().equals(cfmasts.get(i + 1).getCustid())) {
								isCheck = false;
								fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(),
										"There is more than one contract with the custid");
								continue;
							}
						}
					}
					if (isCheck) {
						cfmast = cfmasts.get(0);
					}
				}
				logger.info("[B4] cfmast result: {}", JsonMapper.writeValueAsString(cfmast));

				try {
					int result = cfMastRepo.makeTransfer(cfmast, msg);
					logger.info("[B4] call procedure result: {}", result);

					if (result == 1) {
						logger.info("[B4] start call update status with key: {}", msg.getCustId());
						fundsTransfer.updateStatus(Constants.STATUS_SUCCESS, msg.getCustId(), null);
					} else {
						fundsTransfer.updateStatus(Constants.STATUS_FAIL, msg.getCustId(), "call procedure fail");
					}
				} catch (Exception e) {
					logger.info("[B4] error when call procedure funds transfer {}", JsonMapper.writeValueAsString(e));
					fundsTransfer.updateStatus(Constants.STATUS_FAIL, cfmast.getCustid(),
							JsonMapper.writeValueAsString(e.getMessage()));
				}
			}
		}
	}

	private List<MsgResponse> saveObjectToDB(List<MsgResponse> listMsg) {
		
		logger.info("[B3] Start mapping object to DB");
		List<MsgResponse> listUnFunds = new ArrayList<MsgResponse>();
		
		List<EFundsTransfer> entities = new ArrayList<EFundsTransfer>();
		for (MsgResponse msg : listMsg) {
			Optional<EFundsTransfer> obj = fundsTransfer.findByCustIdAndSendDate(msg.getCustId(), msg.getSendTime());
			logger.info("[B3] check obj is exist: {}", JsonMapper.writeValueAsString(obj));
			if (obj.isPresent()) continue;
			
			EFundsTransfer entiti = new EFundsTransfer();
			entiti.setId(UUID.randomUUID().toString());
			entiti.setSender(msg.getFrom());
			entiti.setSubject(msg.getSubject());
			entiti.setAmount(msg.getAmount());
			entiti.setBefBalance(msg.getBefBalance());
			entiti.setAvlBalance(msg.getAvlBalance());
			entiti.setSendDate(msg.getSendTime());
			if (!StringUtil.isEmty(msg.getTxDate())) {
				entiti.setTxDate(Utils.convertStringToDate(msg.getTxDate()));
			}
			entiti.setCustId(msg.getCustId());
			entiti.setCreateDate(new Date(Calendar.getInstance().getTimeInMillis()));
			entiti.setContent(msg.getContent());
			entiti.setStatus(Constants.STATUS_PROCESSING);
			
			entities.add(entiti);
			listUnFunds.add(msg);
		}
		
		logger.info("[B3] Start save object to DB size: {} value: {}", entities.size(), JsonMapper.writeValueAsString(entities));
		if (entities.isEmpty()) new ArrayList<MsgResponse>(); 
		try {
			fundsTransfer.saveAll(entities);
			return listUnFunds;
		} catch (Exception e) {
			logger.info("[B3] save object to Db fail");
			return new ArrayList<MsgResponse>();
		}
	}

	private MsgResponse getDataFromMessage(Message message, String sender) {
			MsgResponse msg = new MsgResponse();
		try {
			msg.setFrom(sender);
			msg.setSubject(message.getSubject());
			msg.setSendTime(message.getSentDate());

			logger.info("[B2] message {}", JsonMapper.writeValueAsString(message.getContent()));

			String result = "";
			if (message.isMimeType("text/plain")) {
				 result = message.getContent().toString();
				 logger.info("--------------[B2] result from message --------: {}", 
						 JsonMapper.writeValueAsString(result));
		    } else if (message.isMimeType("multipart/*")) {
		        MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
		         result = getTextFromMimeMultipart(mimeMultipart);
		    } else {
		         result = message.getContent().toString();
		    }
			
			msg = getContentFromMail(msg, result);
			logger.info("[B2] Data from message : {}", JsonMapper.writeValueAsString(msg));
		} catch (MessagingException e1) {
			logger.info("[B2] MessagingException : {}", JsonMapper.writeValueAsString(e1));
		} 
		catch (IOException e) {
			logger.info("[B2] IOException : {}", JsonMapper.writeValueAsString(e));
		}
		
		return msg;
	}

	private MsgResponse getContentFromMail(MsgResponse msg, String result) {
		logger.info("[B2] result {}", JsonMapper.writeValueAsString(result));
		int isCredited = result.indexOf("CREDITED");
		int indexBl = result.indexOf(Constants.TEXT_BL_AMOUNT);
		int indexAmount = result.indexOf(Constants.TEXT_AMOUNT_TRANSACTION);
		int indexDate = result.indexOf(Constants.TEXT_TXDATE);
		int indexAvl = result.indexOf(Constants.TEXT_AVL_AMOUNT);
		int indexContent = result.indexOf(Constants.TEXT_CONTENT);
		int endContent = result.indexOf(Constants.TEXT_END_CONTENT);
		
		if (isCredited == -1) return msg;
		
		if (indexBl != -1 && indexAmount != -1 && indexDate != -1 && indexAvl != -1 && indexContent != -1) {
			String blAmount = result.substring(indexBl + Constants.TEXT_BL_AMOUNT.length(), indexAmount).trim();
			String amount = result.substring(indexAmount + Constants.TEXT_AMOUNT_TRANSACTION.length(), indexDate).trim();
			String txDate = result.substring(indexDate + Constants.TEXT_TXDATE.length(), indexAvl).trim();
			String avlAmount = result.substring(indexAvl + Constants.TEXT_AVL_AMOUNT.length(), indexContent).trim();
			String content = result.substring(indexContent + Constants.TEXT_CONTENT.length(), endContent).trim();
			content = new String(content.getBytes(), StandardCharsets.UTF_8);
			logger.info("[B21] [blAmount] {} [amount] {} [txDate] {} [avlAmount] {} [content] {}", 
					blAmount, amount, txDate, avlAmount, content);
			
			int indexSub = content.toUpperCase().indexOf(Constants.TEXT_CONTENT_FUNDS);
			int endSub = content.toUpperCase().indexOf(Constants.TEXT_END_FUNDS);
			if (endSub == -1) {
				endSub = content.toUpperCase().indexOf(Constants.TEXT_END_FUNDS2);
			}
			String custId = null;
			logger.info("[B21] indexSub {}  endSub {}", indexSub, endSub);
			if (indexSub != -1 && endSub != -1) {
				custId = content.toUpperCase().substring(indexSub + Constants.TEXT_CONTENT_FUNDS.length(), endSub).trim();
				logger.info("[B21] custId from message : {}", JsonMapper.writeValueAsString(custId));
			}

			msg.setCustId(custId);
			msg.setBefBalance(Utils.parseLong(blAmount));
			msg.setAmount(Utils.parseLong(amount));
			msg.setAvlBalance(Utils.parseLong(avlAmount));
			msg.setTxDate(txDate);
			msg.setContent(content);
		}
		
		return msg;
	}

	private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			logger.info("bodyPart {}", bodyPart.getContent());
			if (bodyPart.isMimeType("text/plain")) {
				logger.info("------------------text/plain---------- {}", JsonMapper.writeValueAsString(bodyPart.getContent().toString()));
				result = result + "\n" + bodyPart.getContent();
				break;
			} else if (bodyPart.isMimeType("text/html")) {
				logger.info("------------------text/html---------- {}", JsonMapper.writeValueAsString(bodyPart.getContent()));
				String html = (String) bodyPart.getContent();
				result = result + "\n" + Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
}
