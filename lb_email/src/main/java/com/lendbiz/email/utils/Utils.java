package com.lendbiz.email.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lendbiz.email.model.exception.BusinessException;

public class Utils {
	public static final Logger logger = LogManager.getLogger(Utils.class);
	
	public static final String SUB_ID = "lendbiz_";

	public static String generateId(int numberOfCharactor) {
		String alpha = "abcdefghijklmnopqrstuvwxyz0123456789"; // a-z

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < numberOfCharactor; i++) {
			int number = randomNumber(0, alpha.length() - 1);
			char ch = alpha.charAt(number);
			sb.append(ch);
		}
		return SUB_ID + sb.toString();
	}

	public static int randomNumber(int min, int max) {
		Random generator = new Random();
		return generator.nextInt((max - min) + 1) + min;
	}

	public static boolean isCompareDate(Date refIdDate, String issueDate) {
		try {
			Date issue = new SimpleDateFormat("dd-MM-yyyy").parse(issueDate);
			Date refDate = new SimpleDateFormat("yyyy-MM-dd").parse(refIdDate.toString());

			if (refDate.compareTo(issue) == 0) {
				return true;
			}

			return false;
		} catch (ParseException e) {
			throw new BusinessException("07", "Can not compare date with date of identity");
		}
	}
	
	public static boolean isCompareDate(Date value) {
		try {
			String issue = new SimpleDateFormat("dd/MM/yyyy").format(value);
			Date sentDate = new SimpleDateFormat("dd/MM/yyyy").parse(issue);
			String ref = new SimpleDateFormat("dd/MM/yyyy").format(new Date(Calendar.getInstance().getTimeInMillis()));
			Date now = new SimpleDateFormat("dd/MM/yyyy").parse(ref);

			if (sentDate.compareTo(now) == 0) {
				return true;
			}

			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static final Long parseLong(String value) {

		try {
			value = value.replace(",", "");
			value = value.replace(".00", "");
			value = value.replace("*", "");
			value = value.replace("<br/>", "");
			value = value.replace("</strong>", "");
			return Long.parseLong(value.trim());
		} catch (Exception e) {
			logger.info("Can not convert amount to number: {}", value);
			return Long.parseLong("-1");
		}
	}
	
	public static final Date convertStringToDate(String value) {
		try {
			 return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(value);
		} catch (ParseException e) {
			throw new BusinessException("08", "Can not convert date from string");
		}
	}
	
	public static final String convertDateToString(Date date) {
		try {
			Date dt = new Date(date.getTime());
			return new SimpleDateFormat("dd/MM/yyyy").format(dt);
		} catch (Exception e) {
			throw new BusinessException("08", "Can not convert date to string");
		}
	}
	
	public static final boolean isNumber(String value) {
		String regex = "[0-9]+";
		return value.matches(regex);
	}
}
